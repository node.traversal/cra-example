/// <reference types="cypress" />

context('Location', () => {
  beforeEach(() => {
    cy.visit(Cypress.env('APP_URL') || 'http://localhost:3000');
  });

  it('cy.location() - get window.location', () => {
    // https://on.cypress.io/location
     cy.get('.App').contains('Learn React');

    cy.percySnapshot();
  });
});
